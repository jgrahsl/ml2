build:
	docker-compose build

run:
	docker-compose run notebook

bash:
	docker-compose run bash
