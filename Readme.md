# Python and azuresdk playground

I wanted to learn how to perform NN training on the cloud. So i built a docker image containing a few prerequisits to get started. Added a few trivial examples gathered from the net to train model locally or remotely on azure cloud.

## Contents

- `Makefile` with following targets
 - `build` for building the docker image
 - `run` for starting a jupyter notebook listening on `127.0.0.1:8888` using password `notebook`
 - `bash` to drop into a bash running inside the container
- `docker-compose.yaml` configuring the docker image as jupyter notebook or bash entry
 - Do not use this directly but use the makefile targets instead
- `/src` contains a few examples, see corresponding section below

## Getting started

### Azure subscription and configuration

- Get a Free Trail on azure (https://azure.microsoft.com/en-us/free/)
- Create a resource of type "Machine Learning"
- Use the "Download config.json" option on the resource overview page to download subscription configuration
- Place the config.json file inside the `/src` folder in this repo

### Running jupyter notebook as control panel

- `make build` to build the image containing
- `make run` to start juptyer notebook
- connect to the notebook with your browser running on the same machine (127.0.0.1:8888)
- password is `notebook`
- pick an example and starting playing

