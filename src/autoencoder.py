#!/usr/bin/env python
# coding: utf-8

import os
import numpy as np
import tensorflow
import matplotlib.pyplot as plt
from tensorflow.keras import losses
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation
from azureml.core import Run

from tensorflow.examples.tutorials.mnist import input_data



model= Sequential()



model.add(Dense(32)





input = np.array([[0.0, 0.0], [0.0, 1.0], [1.0, 0.0], [1.0, 1.0]])
YOR = np.array([[0.], [1.], [1.], [1.]])
YAND=np.array([[0], [0], [0], [1.]])
YXOR=np.array([[0.], [1.], [1.], [0.]])

model= Sequential()
model.add(Dense(4, input_dim=2))
model.add(Activation('sigmoid'))
model.add(Dense(6))
model.add(Activation('sigmoid'))
model.add(Dense(6))
model.add(Activation('sigmoid'))
model.add(Dense(1))
model.add(Activation('sigmoid'))


#l = 'binary_crossentropy'
l = tensorflow.keras.losses.BinaryCrossentropy()
#l = tensorflow.keras.losses.Huber(delta=1)

model.compile(optimizer='rmsprop',loss=l, metrics=['mean_squared_error','mean_absolute_error','accuracy'])
history = model.fit(input,YXOR,epochs=5000,verbose=False)
print(history.history.keys())
score = model.evaluate(input,YXOR)

plt.plot(history.history['loss'], label='loss (testing data)')
plt.plot(history.history['mean_squared_error'], label='MSE (mse)')
plt.plot(history.history['mean_absolute_error'], label='MAE (mae)')
plt.title('Loss/MSE/MAE')
plt.ylabel('value')
plt.xlabel('No. epoch')
plt.legend(loc="upper right")
#plt.savefig("loss.png")

run = Run.get_context()
run.log('accuracy', score[0])
run.log_image('loss', plot=plt)
